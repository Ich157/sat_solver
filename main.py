import numpy as np
def find_next_empty(grid):
        for r in range(9):
                for c in range(9):
                        if grid[r][c] == 0:
                                return r, c
        return None, None


def is_valid(grid, fillIn, row, col):
        if fillIn in grid[row]:
                return False
        col_values = [grid[i][col] for i in range(9)]
        if fillIn in col_values:
                return False
        row_start = (row // 3) * 3
        col_start = (col // 3) * 3

        for r in range(row_start, row_start + 3):
                for c in range(col_start, col_start + 3):
                        if grid[r][c] == fillIn:
                                return False
        return True

def solve(grid):
        row, col = find_next_empty(grid)
        if row == None:
                return grid
        for fillIn in range(9):
                if is_valid(grid, fillIn + 1, row, col):
                        grid[row][col] = fillIn + 1
                        if solve(grid):
                                return grid
                grid[row][col] = 0
        return None

if __name__ == '__main__':
   grid = [[0,9,4,0,0,0,1,3,0],
           [0,0,0,0,0,0,0,0,0],
           [0,0,0,0,7,6,0,0,2],
           [0,8,0,0,1,0,0,0,0],
           [0,3,2,0,0,0,0,0,0],
           [0,0,0,2,0,0,0,6,0],
           [0,0,0,0,5,0,4,0,0],
           [0,0,0,0,0,8,0,0,7],
           [0,0,6,3,0,4,0,0,8]]
   print(np.matrix(grid))
   grid = solve(grid)
   print(np.matrix(grid))








# See PyCharm help at https://www.jetbrains.com/help/pycharm/
